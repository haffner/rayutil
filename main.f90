program main
  use bodhaine_rayleigh
  implicit none
  
  integer(kind=4), parameter :: FORWARD_MAXLAMBDAS = 10000
  real(kind=4), dimension(FORWARD_MAXLAMBDAS) :: FORWARD_LAMBDAS
  real(kind=8), dimension(FORWARD_MAXLAMBDAS) :: RAYLEIGH_XSEC, RAYLEIGH_DEPOL, KING_FACTOR
  real(kind=8), dimension(FORWARD_MAXLAMBDAS) :: TAURAY, TAURAY2, NOUTM1
  integer(kind=4) :: FORWARD_NLAMBDAS
  real(kind=8) :: CO2_PPMV_MIXRATIO, MCO2
  real(kind=8), parameter  :: DTOR=ACOS(-1.0D0)/180.0D0
  real(kind=8) :: LAT, COS2LAT,GNOT, GEE, Z, ZC
  real(kind=8), parameter ::   NA = 6.0221367D+23, MAIR_NOCO2=28.9595D0
  real(kind=8) :: PRESSURE, MAIR
  integer(kind=4) :: i, stat, num_arg
  CHARACTER(len=128) :: arg
  CHARACTER(len=128) :: outfile
  real(kind=4) :: lam_start, lam_stop, lam_step 
  
  i = 0
  DO
     num_arg = iargc()
     CALL get_command_argument(i, arg)
     IF (LEN_TRIM(arg) == 0) EXIT
     select case (i)
     case(1)     
        read(arg,*,iostat=stat) lam_start
     case(2)
        read(arg,*,iostat=stat) lam_stop
     case(3)
        read(arg,*,iostat=stat) lam_step
     case default
     endselect
     i = i+1
  END DO

  if (num_arg == 4) then
     CALL get_command_argument(4, arg)
     read(arg,*,iostat=stat) outfile        
  else
     outfile='data.out'
  endif
  
  CO2_PPMV_MIXRATIO = 360.0d0
  FORWARD_NLAMBDAS = 1.0*(lam_stop-lam_start)/lam_step+1
  DO i=1, FORWARD_NLAMBDAS
     FORWARD_LAMBDAS(i) = lam_start + lam_step*(i-1)
  ENDDO
  
  CALL RAYLEIGH_FUNCTION( &
       FORWARD_MAXLAMBDAS, CO2_PPMV_MIXRATIO, &
       FORWARD_NLAMBDAS,   FORWARD_LAMBDAS, &
       RAYLEIGH_XSEC, RAYLEIGH_DEPOL, NOUTM1 )

  KING_FACTOR = (6.0d0 + 3.0d0*RAYLEIGH_DEPOL)/(6.0d0 - 7.0d0*RAYLEIGH_DEPOL)

  ! scattering cross-sections reported in units cm^2
  !RAYLEIGH_XSEC = 1.0D-4*RAYLEIGH_XSEC ! -> m^2 

!  call optical_depth( &
!       PRESSURE,

  Z=0.0D0
  LAT=45.0D0
  PRESSURE = 1.0D+3 * 1013.25 ! (dyn cm-2)
  
  COS2LAT=COS(2.0D0*LAT*DTOR)
  
  ! EQ (11)
  GNOT = 980.6160D0 * (1 -  0.0026373D0 * COS2LAT + &
       0.0000059D0 * COS2LAT*COS2LAT)

  ! EQ (26)
  ZC = 0.73737 * Z + 5517.56D0

  ! EQ (10)
  GEE = GNOT - (3.085462D-4 + 2.27D-7 * COS2LAT) * ZC &
       + (7.254D-11 + 1.0D-13 * COS2LAT) * ZC*ZC &
       - (1.517D-17 + 6.0D-20 * COS2LAT) * ZC*ZC*ZC

  MCO2 = 1.0D-6 * CO2_PPMV_MIXRATIO
  MAIR = 15.0556D0 * MCO2 + MAIR_NOCO2 

  TAURAY = RAYLEIGH_XSEC*PRESSURE*NA/MAIR/GEE

  LAT=19.533D0
  Z=3400.0D0
  PRESSURE = 1.0D+3 * 680.0D0 ! (dyn cm-2)
  
  COS2LAT=COS(2.0D0*LAT*DTOR)
  
  ! EQ (11)
  GNOT = 980.6160D0 * (1 -  0.0026373D0 * COS2LAT + &
       0.0000059D0 * COS2LAT*COS2LAT)

  ! EQ (26)
  ZC = 0.73737 * Z + 5517.56D0

  ! EQ (10)
  GEE = GNOT - (3.085462D-4 + 2.27D-7 * COS2LAT) * ZC &
       + (7.254D-11 + 1.0D-13 * COS2LAT) * ZC*ZC &
       - (1.517D-17 + 6.0D-20 * COS2LAT) * ZC*ZC*ZC

  MCO2 = 1.0D-6 * CO2_PPMV_MIXRATIO
  MAIR = 15.0556D0 * MCO2 + MAIR_NOCO2 

  TAURAY2 = RAYLEIGH_XSEC*PRESSURE*NA / MAIR / GEE

  open(unit=22,status='unknown',file=outfile)
  write(22,'(A9,2X,A10,2X,A10,2X,A10,2X,A11,2X,A9)') 'lambda.nm','xsec.cm2','tau_sea','tau_MLO','King factor','1e8*(n-1)'
  DO i=1, FORWARD_NLAMBDAS
     WRITE (22,'(2X,F7.2,2X,1p E10.4,2X,E10.4,2X,E10.4,4X, 0p F7.5,5X,F7.1)') FORWARD_LAMBDAS(I), RAYLEIGH_XSEC(I), &
          TAURAY(I), TAURAY2(I), KING_FACTOR(I), 1.0D+8*NOUTM1(I)
  ENDDO
  close(22)
  
end program main






