# Rayeleigh utility

March 2021
D. Haffner

Calculate Rayleigh optical depth and scattering cross-section, depolarization ratio, King factor,
and index of refraction using the formulae in Bodhaine et al., 1999 and Peck and Reeder, 1972.

Wavelengths are vacuum.  CO2 concentration is fixed at 360 ppm for consistency with Table 3 of Bodhaine et al. 

Comments are always welcome.

## Usage:

  To compile, run make. Execute the program as follows:

  `rayutil start stop step [outfile]`

```
  start: start wavelength (nm)
  stop: end wavelength (nm)
  step: wavelength step (nm)
  outfile: output filename (optional)
```
  If no output filename is supplied the data are written to data.out 
  Rayleigh scattering cross-section is reported in cm2.

## Test case:

  'make test' generates table 3 of in Bodhaine et al. as a unit test,

  `rayutil 250 1000 5 bodhaine.dat`

  Output bodhaine.dat is compared to table_3.dat in the test directory.

## References:

   Bodhaine, B. A., Wood, N. B., Dutton, E. G., and Slusser, J. R. (1999). On Rayleigh Optical Depth Calculations.
   J. Atmos. Ocean. Technol. 16, 11, 1854-1861

   Peck, E. R. and Reeder, K. (1972). Dispersion of Air*. J. Opt. Soc. Am. 62, 8, 958-962
