.PHONY: all test clean

build:
	gfortran -c bodhaine_rayleigh.f90
	gfortran -o rayutil main.f90 bodhaine_rayleigh.o

test:
	test/test.sh

clean:
	rm rayutil *.o *.mod
	rm bodhaine.out

all: build test
